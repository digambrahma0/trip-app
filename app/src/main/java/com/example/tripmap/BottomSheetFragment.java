package com.example.tripmap;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.tripmap.Model.ScooterModel;


public class BottomSheetFragment extends BottomSheetDialogFragment {

    private ButtonClickListener buttonClickListener;
    private Button unlockBtn;
    public Button cancel_ride;
    public BottomSheetFragment() {
    }


    interface ButtonClickListener{
        void onButtonClicked(ScooterModel position);
        void onRideCanceled();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet, container, false);
        unlockBtn =  v.findViewById(R.id.unlockBtn);
        cancel_ride = v.findViewById(R.id.cancelId);
        final ScooterModel scooterModel = (ScooterModel) getArguments().getSerializable("Id");
        Log.d("onCreateView", "onCreateView: " + scooterModel.getId());

        View.OnClickListener vi = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.cancelId:
                        buttonClickListener.onRideCanceled();
                        break;
                    case R.id.unlockBtn:
                        buttonClickListener.onButtonClicked(scooterModel);
                }

            }
        };

        unlockBtn.setOnClickListener(vi);
        cancel_ride.setOnClickListener(vi);

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        buttonClickListener = (BottomSheetFragment.ButtonClickListener) context;
    }
}