package com.example.tripmap;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.tripmap.Model.ScooterModel;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class KeylessFragment extends Fragment implements ScooterAdapter.OnCardListener, RequestJsonData.OnDrawPolyline, OnMapReadyCallback {
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private ImageView mGps;
    private TextView locationSearch;
    private TextView walkingDistance;

    private PlacesClient placesClient;
    private List<AutocompletePrediction> predictionList;
    private View mapView;
    private FragmentActivity myContext;

    private static final String TAG = "Main Activity";
    private static final float DEFAULT_ZOOM = 15f;
    private Location mLastKnownLocation;
    private LocationCallback locationCallback;

    private PolylineOptions polyLineOptions;
    private RequestQueue mQueue;
    private RequestQueue twoQueue;
    private RequestJsonData requestJsonData;

    private RecyclerView recyclerView;
    private ScooterAdapter scooterAdapter;

    private ArrayList<ScooterModel> scooterList = new ArrayList<>();
    private Map<Integer, Marker> mark;
    private Map<Marker, Integer> kram;

    private Polyline polylineFinal;
    private List<LatLng> m_path;
    private SnapHelper helper = new LinearSnapHelper();
    private ShimmerFrameLayout shimmerFrameLayout;
    private RecyclerView.LayoutManager mLayoutManager;
    private RelativeLayout searchPlace;
    private Intent intent;
    private String mlocationGet;

    private FloatingActionButton fab1;
    private FloatingActionButton fab2;
    @Override
    public void onAttach(Context context) {
        myContext = (FragmentActivity) context;
        super.onAttach(context);
    }

    private void jsonParse() {
        Log.d(TAG, "jsonParse: running ");
        String url = "http://tript.pe.hu/api/vehicle/list";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("list");
                    Log.d(TAG, "onResponse: fetching..." + jsonArray);
                        for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject list = jsonArray.getJSONObject(i);

                        String lat = list.getString("lat");
                        String lng = list.getString("lng");
                        double latitudeOfBike = Double.parseDouble(lat);
                        double longitudeOfBike = Double.parseDouble(lng);

                        Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitudeOfBike, longitudeOfBike)).icon(bitmapDescriptorFromVector(myContext, R.drawable.ic_bike)));
                        mark.put(i, marker);
                        kram.put(marker, i);
                        String battery = list.getString("battery");
                        String name = list.getString("name");
                        String id = list.getString("id");

                        ScooterModel scooterModel = new ScooterModel();
                        scooterModel.setBattery(battery);
                        scooterModel.setName(name);
                        scooterModel.setId(id);
                        scooterList.add(scooterModel);
                        Log.d(TAG, "onResponse: " + scooterList.get(i));
                    }
                        shimmerFrameLayout.stopShimmer();
                        shimmerFrameLayout.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);


                    if (intent != null) {
                        Log.d(TAG, "onResponse: CHECKINGGGG");
                        mlocationGet = (String) intent.getSerializableExtra("LocationData");
                        if (mlocationGet != null) {
                            Log.d(TAG, "onResponse: CHECKING@@@@@");
                            geoLocate(mlocationGet);
                        }else {
                            Log.d(TAG, "onResponse: CHECKING222222");
                            mLayoutManager.scrollToPosition(0);
                            onCardListener(0);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        shimmerFrameLayout.stopShimmer();
                        shimmerFrameLayout.setVisibility(View.GONE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                        builder.setMessage("No Internet Connection found!! Please connect to the internet and then open the app again")
                                .setTitle("No Internet Connection");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) { }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }, 5000);

                Log.d(TAG, "onErrorResponse: running");
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

    private void markerListener() {
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                Log.d(TAG, "onMarkerClick: running");
                final LatLng origin = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                final LatLng dest = marker.getPosition();

                String url = getUrl(origin, dest, "walking");
                Log.d(TAG, "onMarkerClick: " + url);
                requestJsonData = new RequestJsonData(origin, dest, marker, url, getContext(), mMap, KeylessFragment.this);
                requestJsonData.RequestData();

                if(kram.get(marker) != null){
                    mLayoutManager.scrollToPosition(kram.get(marker));
                }
                return false;
            }
        });
    }

    @Override
    public void onCardListener(final int position) {
        if (position < scooterList.size()) {
            final LatLng origin = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
            final LatLng dest = mark.get(position).getPosition();
            Log.d(TAG, "onCardListener: " + origin + " " + dest);
            Marker marker = mark.get(position);
            String url = getUrl(origin, dest, "walking");
            Log.d(TAG, "onCardListener: " + url);
            requestJsonData = new RequestJsonData(origin, dest, marker, url, getContext(), mMap, KeylessFragment.this);
            requestJsonData.RequestData();
        }
    }

    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode
        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + "AIzaSyCb1FVONY1E825yACDsKE-fV0KZW7RtwN8";
        return url;
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void geoLocate(String searchString) {
        Log.d(TAG, "geoLocate: geolocating");
        Geocoder geocoder = new Geocoder(getContext());
        List<Address> list = new ArrayList<>();
        try {
            list = geocoder.getFromLocationName(searchString, 1);
        } catch (IOException e) {
            Log.e(TAG, "geoLocate: IOException: " + e.getMessage());
        }
        if (list.size() > 0) {
            Address address = list.get(0);
            Log.d(TAG, "geoLocate: found a location: " + address.toString());
           // mMap.addMarker(new MarkerOptions().position(new LatLng(address.getLatitude(), address.getLongitude())).title(""));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(address.getLatitude(), address.getLongitude()), DEFAULT_ZOOM));
        }
    }

    @SuppressLint("MissingPermission")
    private void getDeviceLocation() {
        mFusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                if (task.isSuccessful() && task.getResult() != null) {
                    mLastKnownLocation = task.getResult();
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), 15f));
                    if (mLastKnownLocation != null) {
                        mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude())));
                    } else {
                        LocationRequest locationRequest = LocationRequest.create();
                        locationRequest.setInterval(10000);
                        locationRequest.setFastestInterval(5000);
                        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                        locationCallback = new LocationCallback() {
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                super.onLocationResult(locationResult);
                                if (locationResult == null) {
                                    return;
                                }
                                mLastKnownLocation = locationResult.getLastLocation();
                                mFusedLocationProviderClient.removeLocationUpdates(locationCallback);
                            }
                        };
                        mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    }
                } else {

                    Toast.makeText(getContext(), "Unable to get the last location", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 51) {
            if (resultCode == RESULT_OK) {
                getDeviceLocation();
            }
        }
    }

    private void hideSoftKeyboard() {
        Log.d(TAG, "hideSoftKeyboard: hide keyboard is running");
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) myContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private static final PatternItem DOT = new Dot();
    private static final PatternItem GAP = new Gap(8);
    private static final List<PatternItem> PATTERN_POLYLINE_DOTTED = Arrays.asList(GAP, DOT);

    @Override
    public void onDrawPolyline(List<LatLng> m_path, PolylineOptions polyline, String walkingDistance) {
        this.m_path = m_path;
        this.polyLineOptions = polyline;
        if (polylineFinal != null) {
            polylineFinal.remove();
        }

        View v = helper.findSnapView(mLayoutManager);
        int pos = mLayoutManager.getPosition(v);
        TextView vt = v.findViewById(R.id.walkId);
        if (pos < scooterList.size()) vt.setText(walkingDistance);

        polylineFinal = mMap.addPolyline(polyLineOptions);

        polylineFinal.setEndCap(new RoundCap());
        polylineFinal.setWidth(16);
        polylineFinal.setColor(0xFF4893DB);
        polylineFinal.setPattern(PATTERN_POLYLINE_DOTTED);
        polylineFinal.setJointType(JointType.ROUND);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_keyless, container, false);
        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        scooterList.clear();
    }

    //method to convert your text to image
    public static Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.0f); // round
        int height = (int) (baseline + paint.descent() + 0.0f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        fabAction();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    View centerView = helper.findSnapView(mLayoutManager);
                    int pos = mLayoutManager.getPosition(centerView);
                    Log.e("Snapped Item Position:", "" + pos);
                    if (pos < scooterList.size()){
                        onCardListener(pos); 
                    }
                }
            }
        });
        intent = (myContext).getIntent();
        mGps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: back to my location");
                getDeviceLocation();
                hideSoftKeyboard();

            }
        });
        locationSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
                Intent intent = new Intent(myContext, SearchActivity.class);
                startActivity(intent);
            }
        });
        intent = myContext.getIntent();
    }

    @Override
    public void btnBookRide(int position) {
        openBottomSheet(position);

    }

    public void openBottomSheet(int position){
        FirstBottomSheetFragment firstBottomSheetFragment = new FirstBottomSheetFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("Id", scooterList.get(position));
        bundle.putSerializable("position", position);
        firstBottomSheetFragment.setArguments(bundle);
        firstBottomSheetFragment.show(myContext.getSupportFragmentManager(), firstBottomSheetFragment.getTag());

    }

    public void init(View view){
        mark = new HashMap<>(50);
        kram = new HashMap<>(50);
        locationSearch = view.findViewById(R.id.input_search);
        walkingDistance = view.findViewById(R.id.walkId);
        shimmerFrameLayout = view.findViewById(R.id.shimmer_effect);

        fab1 = view.findViewById(R.id.fab1);
        fab2 = view.findViewById(R.id.fab2);
        recyclerView =  view.findViewById(R.id.recycler);
        mGps = view.findViewById(R.id.ic_gps);

        mQueue = Volley.newRequestQueue(myContext);
        twoQueue = Volley.newRequestQueue(myContext);

        scooterAdapter = new ScooterAdapter(myContext, scooterList, this);
        recyclerView.setAdapter(scooterAdapter);
        mLayoutManager = new LinearLayoutManager(myContext, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        helper.attachToRecyclerView(recyclerView);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
    }

    private void fabAction(){
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(myContext, "Fab1 is tabbed", Toast.LENGTH_SHORT).show();
            }
        });
        fab1.setImageBitmap(textAsBitmap("City", 40, Color.WHITE));
        fab2.setImageBitmap(textAsBitmap("Load", 40, Color.WHITE));
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(myContext, "Fab2 is tabbed", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(myContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(myContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mMap.moveCamera(CameraUpdateFactory.scrollBy(1, 10));

                //check if gps is enabled
                LocationRequest locationRequest = LocationRequest.create();
                locationRequest.setInterval(10000);
                locationRequest.setFastestInterval(5000);
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                        .addLocationRequest(locationRequest);
                SettingsClient client = LocationServices.getSettingsClient(myContext);
                Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

                task.addOnSuccessListener(myContext, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        getDeviceLocation();
                        markerListener();
                    }
                });

                task.addOnFailureListener(myContext, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ResolvableApiException) {
                            // Location settings are not satisfied, but this can be fixed
                            // by showing the user a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                ResolvableApiException resolvable = (ResolvableApiException) e;
                                resolvable.startResolutionForResult(myContext,
                                        51);
                            } catch (IntentSender.SendIntentException sendEx) {
                                // Ignore the error.

                                sendEx.printStackTrace();
                            }
                        }
                    }
                });
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            myContext, R.raw.mapstyle));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
        jsonParse();
    }

}

