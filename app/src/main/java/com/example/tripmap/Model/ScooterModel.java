package com.example.tripmap.Model;

import java.io.Serializable;

public class ScooterModel implements Serializable {

    private String battery;
    private String name;
    private String id;

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
