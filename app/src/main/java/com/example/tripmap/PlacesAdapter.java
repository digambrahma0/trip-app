package com.example.tripmap;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.PlacesViewHolder>{

    private static final String TAG = "PlacesAdapter";
    private Context mContext;
    private List<String> location;

    public PlacesAdapter(Context mContext, List<String> location) {
        this.mContext = mContext;
        this.location = location;
    }

    static class PlacesViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;

        public PlacesViewHolder(View v){
            super(v);
            textView = v.findViewById(R.id.address);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull PlacesViewHolder placesViewHolder, int i) {

        Log.d(TAG, "onBindViewHolder: started on position "+i);

        String location = this.location.get(i);

        placesViewHolder.textView.setText(location);

    }

    @Override
    public int getItemCount() {
        return ((location != null)&&(location.size() != 0))?location.size():0;
    }

    @NonNull
    @Override
    public PlacesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        Log.d(TAG, "onCreateViewHolder: called");

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.text_address, viewGroup, false);

        return new PlacesViewHolder(view);
    }


}