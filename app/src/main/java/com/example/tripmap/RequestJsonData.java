package com.example.tripmap;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RequestJsonData {
    private static final String TAG = "Request Json Data";
    private RequestQueue twoQueue;
    private LatLng origin;
    private LatLng dest;
    private Polyline polylineFinal;
    private PolylineOptions polyLineOptions;
    private GoogleMap mMap;
    private Marker marker;
    String url;
    List<LatLng> m_path;
    OnDrawPolyline onDrawPolyline;
    String timing;
    String walkingDistance;

    public RequestJsonData(LatLng origin, LatLng dest, Marker marker, String url, Context context, GoogleMap mMap, OnDrawPolyline onDrawPolyline) {
        this.origin = origin;
        this.dest = dest;
        this.marker = marker;
        this.url = url;
        twoQueue = Volley.newRequestQueue(context);
        this.mMap = mMap;
        this.onDrawPolyline = onDrawPolyline;

    }

    interface OnDrawPolyline{
        void onDrawPolyline(List<LatLng> m_path, PolylineOptions polyline, String walkingDistance );
    }

    public void RequestData(){
        JsonObjectRequest request2 = new JsonObjectRequest(Request.Method.GET, this.url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
               try {
                       JSONArray routesArray = response.getJSONArray("routes");
                       JSONObject route = routesArray.getJSONObject(0);
                       Log.d(TAG, "onResponse: fetching..." + route);

                       if (route.has("overview_polyline") && !route.isNull("overview_polyline")) {
                           JSONObject m_poly = route.getJSONObject("overview_polyline");
                           if (m_poly.has("points") && !m_poly.isNull("points")) {
                               String enc_points = m_poly.getString("points");
                               m_path = decodePoly(enc_points);

                               polyLineOptions = new PolylineOptions().geodesic(true).addAll(m_path);

                               if (polylineFinal != null){
                                   polylineFinal.remove();
                               }
                               animateCamera(m_path);

                           }

                       JSONArray legs = route.getJSONArray("legs");
                       JSONObject leg = legs.getJSONObject(0);
                       JSONObject duration = leg.getJSONObject("duration");
                       timing = duration.getString("text");
                       Log.d(TAG, "onResponse: " + timing);
                       marker.setTitle(timing);
                       walkingDistance = timing;
                       marker.showInfoWindow();

                       onDrawPolyline.onDrawPolyline(m_path, polyLineOptions, walkingDistance);
                   }
               } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        twoQueue.add(request2);

    }


    private void animateCamera(List<LatLng> m_path) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for(LatLng coord : m_path){
            builder.include(coord);
        }
        final LatLngBounds m_bounds = builder.build();
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(m_bounds, 6));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Log.d(TAG, "run: ZOOOMING");
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(m_bounds, 450));
            }
        }, 800);
    }
    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }


}

