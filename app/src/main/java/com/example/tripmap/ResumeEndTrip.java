package com.example.tripmap;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class ResumeEndTrip extends AppCompatActivity {
    private Button resume_trip;
    private Button end_trip;
    private RequestQueue requestEnd;
    private RequestQueue requestResume;
    private RequestQueue getRequestResume;
    private int position;
    private static final String TAG = "ResumeEndTrip";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume_end_trip);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        position = (Integer) bundle.getSerializable("Position");
        Log.d(TAG, "onCreate: CHECKING POSITION" + position);
        resumeBtnInit();
        endBtnInit();
    }

    public void sendResumeRequest(){

        final String url = "http://tript.pe.hu/api/vehicle/starttrip/"+(position);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "onResponse: "+response+" "+url);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("KeylessFragment", "onErrorResponse: running");
                error.printStackTrace();
            }
        });

        getRequestResume.add(request);

    }

    public void setResume_trip(){

        final String url = "http://tript.pe.hu/api/vehicle/starttrip/"+(position);
        JsonObjectRequest jsonObject = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "onResponse: "+response+" "+url);
                try {
                    JSONObject jsonObject1 = response.getJSONObject("vehicle");
                    String status = jsonObject1.getString("status");
                    Log.d(TAG, "onResponse: "+status);

                    if (!status.equals("3")) Toast.makeText(ResumeEndTrip.this, "The bike is not paused", Toast.LENGTH_SHORT).show();
                    else{
                        sendResumeRequest();
                    }

                }catch (JSONException e){}

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("KeylessFragment", "onErrorResponse: running");
                error.printStackTrace();
            }
        });

        requestResume.add(jsonObject);

    }

    public void resumeBtnInit(){

        resume_trip = findViewById(R.id.resume_trip);
        requestResume = Volley.newRequestQueue(this);
        getRequestResume = Volley.newRequestQueue(this);

        resume_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResume_trip();
            }
        });

    }

    public void setEnd_trip(){

        final String url = "http://tript.pe.hu/api/vehicle/endtrip/"+(position)+"/1";
        Log.d(TAG, "setEnd_trip: "+position);

        JsonObjectRequest jsonObject = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "onResponse: "+response+" "+url);
                BluetoothAdapter.getDefaultAdapter().disable();
                finish();
                Intent intent = new Intent(ResumeEndTrip.this, MapsActivity.class);
                intent.putExtra("BluetoothOff", "OFF");
                startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("KeylessFragment", "onErrorResponse: running");
                error.printStackTrace();
            }
        });
        requestEnd.add(jsonObject);

    }

    public void endBtnInit(){

        end_trip = findViewById(R.id.end_trip);
        requestEnd = Volley.newRequestQueue(this);

        end_trip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEnd_trip();
            }
        });

    }


    @Override
    public void onBackPressed() {

        Toast.makeText(this, "Sorry! You cannot go back. Please End Trip first",Toast.LENGTH_LONG).show();

    }
}