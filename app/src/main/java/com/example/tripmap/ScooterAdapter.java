package com.example.tripmap;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.tripmap.Model.ScooterModel;

import java.util.ArrayList;

public class ScooterAdapter extends RecyclerView.Adapter<ScooterAdapter.ScooterViewHolder>  {

    private OnCardListener onCardListener;
    private ArrayList<ScooterModel> scooterModel;
    TextView scooter_end_message;

    LayoutInflater inflater;

    public ScooterAdapter(Context context, ArrayList<ScooterModel> scooterModel, OnCardListener onCardListener) {
        this.onCardListener = onCardListener;
        this.scooterModel = scooterModel;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ScooterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == R.layout.recycler_item_finished){
            view = inflater.inflate(R.layout.recycler_item_finished, parent, false);
        }
        else {
            view = inflater.inflate(R.layout.recycler_item, parent, false);
        }
        return new ScooterViewHolder(view, onCardListener);
    }

    @Override
    public void onBindViewHolder(ScooterViewHolder holder, final int position) {
        if (position < scooterModel.size()) {
            holder.batteryview.setText(scooterModel.get(position).getBattery());
            holder.nameview.setText(scooterModel.get(position).getName());

            final View.OnClickListener btnBookRide = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onCardListener.btnBookRide(position);
                }
            };
            holder.btnBookRide.setOnClickListener(btnBookRide);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return  (position == scooterModel.size()) ? R.layout.recycler_item_finished:R.layout.recycler_item;
    }

    @Override
    public int getItemCount() {
        return scooterModel.size() + 1;
    }



    public class ScooterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView batteryview;
        TextView nameview;
        OnCardListener onCardListener;
        Button btnBookRide;
        TextView time;

        public ScooterViewHolder(View itemView, OnCardListener onCardListener) {
            super(itemView);
            batteryview = itemView.findViewById(R.id.tx1);
            nameview = itemView.findViewById(R.id.tx2);
            btnBookRide = itemView.findViewById(R.id.btnBookRide);
            time = itemView.findViewById(R.id.walkId);
            this.onCardListener = onCardListener;
            itemView.setOnClickListener(this);
            scooter_end_message = itemView.findViewById(R.id.scooter_end_message);
        }

        @Override
        public void onClick(View v) {
            onCardListener.onCardListener(getAdapterPosition());
        }
    }


    public interface OnCardListener {
        void onCardListener(int position);
        void btnBookRide(int position);
    }
}
