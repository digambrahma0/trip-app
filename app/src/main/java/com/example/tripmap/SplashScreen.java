package com.example.tripmap;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import afu.org.checkerframework.checker.nullness.qual.NonNull;

public class SplashScreen extends AppCompatActivity {

    private boolean mLocationPermissionGranted = false;
    public static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    public static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;

    private String[] permission = {FINE_LOCATION, COARSE_LOCATION};
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;

        switch (requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0){
                    for (int i=0; i<grantResults.length; i++){
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            mLocationPermissionGranted = false;
                            return;
                        }
                    }
                    mLocationPermissionGranted = true;
                    Intent intent = new Intent(this, MapsActivity.class);
                    startActivity(intent);
//                    finish();

                }
                if(permissions.length > 0){
                    for (int i=0; i<permissions.length; i++){
                        if (permissions[i].equals(PackageManager.PERMISSION_GRANTED)){
                            mLocationPermissionGranted = false;
                            return;
                        }
                    }
                }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){//Fine location Permission grant check
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(), COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){//Coarse location permission check
                mLocationPermissionGranted = true;
//                initMap();//initialising the map if the permission is granted
                Handler handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(SplashScreen.this, MapsActivity.class);
                        startActivity(intent);
//                        finish();
                    }
                },500);
//                finish();
            }
        }
        if (!mLocationPermissionGranted){
            ActivityCompat.requestPermissions(this, permission, LOCATION_PERMISSION_REQUEST_CODE);

    }
    }
}
